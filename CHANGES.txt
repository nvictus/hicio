2014-12-?? nvictus
v0.1a
	* modular plugin architecture
		* converters for tsv, bedpe and numpy binary files
		* import/export for hdf5 containers
	* console entry point

2014-12-26  nvictus
v0.2
	* Two separate plugin registries: format (single heatmaps) and store (containers)
	* Added new store plugin for matlab MAT files
