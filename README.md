# Hi-C I/O #

Hi-C heatmap schema and conversion tools

Development
-----------

This distribution provides both a Python library `hicio` and command line script (console entry point), also called `hicio`.

To install the package in developer mode, clone and

```bash
$ cd hicio
$ pip install -e .
```

This will create and add the console entry point to your path. To remove the entry point and the symlinks from site-packages just `pip uninstall hicio`.

Schema version 0.2
------------------

We define Hi-C heatmaps in the most general form as any 2D map (integer or real) along two genomic coordinate axes, coarse-grained into integer-sized **genomic bins**, and spanning two defined genomic **regions**. 

- Regions must comprise **contiguous sequences** of genomic bins. 

- Regions may span more than one chromsome.

- The regions assigned to the two axes of a heatmap need not be the same.

- While genomic bins are usually uniform in size, this schema enforces no such limitation.

Here's a JSON-esque description of the schema:

```json
{
	"title": "heatmap",
	"description": "schema for a Hi-C heatmap",
	"required": ["version", "info", "frame", "matrix"],
	"properties": {
		// Schema version
		"version": "string",

		// Matrix storage information and heatmap metadata
		"info": {
			"type": "object",
			"required": ["dtype", "mime"],
			"properties": {
				"ftype": "string",
				"mime": "string",
				"dtype": "string",
				"assembly": "string",
				...
			}
		},

		// Genomic bin information
		"frame": {
			"type": "table",
			"required": ["chrom", "start", "end", "axis", "missing"],
			"columns": {
				"chrom": "string",
				"start": "integer",
				"end": "integer",
				"axis": "integer",
				"missing": "boolean",
				"bias": "float",
				...
			}
		},

		// Heatmap pixel values
		"matrix": {
			"type": "array",
			"ndim": 2
		}
	}
}
```

The central "exchange" representation for these three data structures will be:

- meta: python dict
- frame: pandas dataframe
- matrix: 2D numpy array    

```
TODO: optionally support sparse matrix in place of ndarray?
```

Manifest
--------
Accompanying a heatmap matrix, in whatever form it is represented, should be two sets of metadata:

1) _meta_: a key-value store describing the data type and file format of the matrix.

2) _frame_: an extensible table describing the genomic bins (start, end, axis, etc.)

Symmetric heatmaps only need to list the coordinates along one axis, whereas asymmetric heatmaps (e.g. _trans_ heatmaps) need to list both.

For simple file-based representations of the heatmap, the manifest is stored as an accompanying manifest file, stored in a BED-like format, which looks like the following:

```
# -*- BEDHM version 1.0 -*-
# dtype=i4
# ftype=tsv
# mime=text/plain
chrom	start	end	axis	missing
chr22	0	1000	0	0
chr22	1000	2000	0	0
chr22	2000	3000	0	0
chr22	3000	4000	0	0
chr22	4000	5000	0	0
chr22	5000	6000	0	0
chr22	6000	7000	0	0
chr22	7000	8000	0	0
chr22	8000	9000	0	0
chr22	9000	10000	0	0
...
```

```
TODO: provide an easy way to generate/validate a manifest from a genomic range and a matrix
```
TODO: develop a [JSON-Schema](http://json-schema.org/) for a JSON-based manifest file.

Formats
-------
File formats for single heatmaps will pair matrix files with manifest files.

For every supported file format, we implement `load` methods to parse manifest and matrix data into python and `dump` methods to serialize the data to disk.

Currently supported formats:

- `tsv`: tab-delimited full matrix format
- `bedpe`: tab-delimited and coordinate-annotated sparse format (bedtools compatible)
- `npy`: NumPy binary format (preserves floating point precision)

```
TODO:
- pickle
- tsv triples
- add gzip option
```

Hi-C I/O supports file type conversion at the command line:

```bash
$ hicio convert example.tsv --format bedpe
```

By default, the manifest file is given the same name as the heatmap followed by the extension "`.bedhm`".

```
pwd
|-- example.tsv
|-- example.tsv.bedhm
|-- example.bedpe
|-- example.bedpe.bedhm
```

Stores
------
Instead of storing heatmaps and manifests in separate files, we may also package collections of them into compressible or binary stores.

Currently supported formats:

- `HDF5`
- MATLAB 'mat' (v5+) files

```
TODO:
- npz
- R
- compressed tar or zip?
```

Hi-C I/O supports extracting heatmaps from stores and packing them into stores:

```bash
$ hicio h5 dump example.h5 --format tsv
$ hicio h5 pack ./data/*.tsv foo.h5
```

```bash
$ hicio h5 list bar.h5
```



