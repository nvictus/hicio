from setuptools import setup, Extension
import hicio

setup(
    name=hicio.__name__,
    version=hicio.__version__,
    author='Nezar Abdennur',
    author_email='nezar@mit.edu',
    license='MIT',
    description='Hi-C data schema and file conversion',
    # long_description='',
    # keywords='sample setuptools development',
    packages=['hicio'],
    classifiers=[
        'Development Status :: 3 - Alpha',
    ],
    # cmdclass = {'build_ext': build_ext},
    # ext_modules = ext_modules,
    install_requires=[
        'numpy',
        'scipy',
        'pandas',
        'docopt',
    ],
    # package_data={
    #     'sample': ['package_data.dat'],
    # },
    #In the following case, 'data_file' will be installed into '<sys.prefix>/my_data'
    # data_files=[('my_data', ['data/data_file'])],
    entry_points={
        'console_scripts': [
            'hicio=hicio.__main__:main',
        ],
    },
)
