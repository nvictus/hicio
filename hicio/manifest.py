from __future__ import division, print_function
import numpy as np
import pandas

from . import __version__


def symmetric_frame(grange):
    chrom, start, stop, binsize = grange
    n_bins = (stop-start) // binsize
    table = pandas.DataFrame(
        data={
            'chrom': [chrom]*n_bins,
            'start': start + np.arange(start, stop, binsize),
            'end': start + np.arange(start+binsize, stop+binsize, binsize),
            'axis': np.zeros(n_bins, dtype=int),
            'missing': np.zeros(n_bins, dtype=int),
        }, 
        columns=['chrom', 'start', 'end', 'axis', 'missing']
    )
    return table


def asymmetric_frame(grange0, grange1):
    chrom0, start0, stop0, binsize0 = grange0
    n_bins0 = (stop0-start0) // binsize0
    chrom1, start1, stop1, binsize1 = grange1
    n_bins1 = (stop1-start1) // binsize1
    table = pandas.DataFrame(
        data={
            'chrom': [chrom0]*n_bins0 + [chrom1]+n_bins1,
            'start': np.r_[start0 + np.arange(0, n_bins0, dtype=int)*binsize0,
                           start1 + np.arange(0, n_bins1, dtype=int)*binsize1],
            'end': np.r_[start0 + np.arange(1, n_bins0+1, dtype=int)*binsize0,
                          start1 + np.arange(1, n_bins1+1, dtype=int)*binsize1],
            'axis': np.r_[np.zeros(n_bins0, dtype=int), 
                          np.ones(n_bins1, dtype=int)],
            'missing': np.zeros(n_bins0 + n_bins1, dtype=int),
        }, 
        columns=['chrom', 'start', 'end', 'axis', 'missing']
    )
    return table


# Character codes for numpy dtypes
# --------------------------------
# http://docs.scipy.org/doc/numpy/reference/arrays.dtypes.html#specifying-and-constructing-data-types
#
# 'b'       boolean
# 'i'       (signed) integer
# 'u'       unsigned integer
# 'f'       floating-point
# 'c'       complex-floating point
# 'S'/'a'   bytes as ascii
# 'U'       unicode
# 'O'       (Python) objects
# 'V'       raw data (void)
#
class Manifest(object):
    header_template = """# -*- BEDHM version {version} -*-
# dtype={dtype}
# ftype={ftype}
# mime={mime}
""".encode('utf-8')

    columns = ['chrom', 'start', 'end', 'axis', 'missing']

    @classmethod
    def from_bedfile(cls, file_):
        with open(file_, 'r') as f:
            args = []
            for i, line in enumerate(f):
                if not line.startswith('#'):
                    break

                if i == 0:
                    header = line.lstrip('# ')
                    if not header.lower().startswith("-*- bedhm"):
                        raise IOError("Invalid manifest.")
                else:
                    args.append(line.lstrip('# ').strip().split('='))
            meta = dict(args)

        with open(file_, 'r') as f:
            for _ in range(i):
                f.readline()
            table = pandas.read_csv(f, sep='\t')

        self = cls(meta)
        self.table = table
        return self

    @classmethod
    def from_dataframe(cls, df, *a, **kw):
        self = cls(*a, **kw)
        df = pandas.concat((self.table, df), join='outer')
        cols = self.columns + list(set(df.columns)-set(self.columns))
        self.table = df[cols]
        return self

    @classmethod
    def from_dict(cls, d, *a, **kw):
        self = cls(*a, **kw)
        df0 = pandas.DataFrame.from_dict(d)
        df = pandas.concat((self.table, df0), join='outer')
        cols = self.columns + list(set(df.columns)-set(self.columns))
        self.table = df[cols]
        return self

    @classmethod
    def from_json(cls, s):
        pass

    def __init__(self, meta=None):
        self.meta = {
            'version': __version__,
            'dtype': 'f8', #i4
            'ftype': 'tsv',
            'mime': 'text/plain',
            'optional_columns': '',
        }
        if meta is not None:
            self.meta.update(meta)
        self.table = pandas.DataFrame(columns=self.columns)

    def __getitem__(self, key):
        return self.meta[key]

    def __setitem__(self, key, val):
        self.meta[key] = val

    def to_bedfile(self, file_):
        with open(file_, 'w') as f:
            f.write(self.header_template.format(**self.meta))
        columns = self.columns + list( set(self.columns) - set(self.table.columns) )
        self.table.to_csv(file_, mode='a', sep='\t', index=False, columns=self.columns)

    def to_json(self):
        pass

    # def read_bed(self, file_):
    #     df = pandas.read_csv(file_, sep='\t', header=None)
    #     self.table = pandas.concat((self.table, df), join='outer')


