from __future__ import division, print_function

def strip_suffix(text, suffix):
    if not text.endswith(suffix):
        return text
    return text[:len(text)-len(suffix)]

def mkdir_p(path):
    # http://stackoverflow.com/questions/600268
    import os
    import errno
    try:
        os.makedirs(path)
    except OSError as e:
        if e.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else: 
            raise

def genomic_range(chrom, start, stop, step=None, n_bins=None):
    # chrom, start, None, step, n_bins
    # chrom, start, stop (assume step=1)
    # chrom, start, stop, step
    # chrom, start, stop, n_bins
    if stop is None:
        if n_bins is None or step is None:
            raise TypeError("Must specify both 'step' and 'n_bins' if 'stop' is not provided")
        else:
            stop = start + n_bins*step

    if step is None:
        if n_bins is None:
            step = 1
        else:
            step = (stop-start) // n_bins
            if step < 1:
                raise ValueError("Can't divide range into {} bins".format(n_bins))

    return (chrom, start, stop, step)
