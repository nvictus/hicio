"""
Hi-C I/O.

Hi-C heatmaps are imported as:

    1) Manifest (object):
        - dictionary describing the matrix file format and dtype
        - dataframe describing the genomic bins
    2) Matrix:
        - numpy ndarray

"""
from __future__ import division, print_function
__version__ = '0.2'

import os
from .format import registry as format_registry
from .store import registry as store_registry
from .utils import genomic_range

__all__ = ['load', 'dump']


def load(filename, format):
    if format in format_registry and format_registry[format].importable:
        io = format_registry[format]
        mf = io.load_manifest(filename+'.bedhm')
        mat = io.load_matrix(filename, mf)
    else:
        raise ValueError("Unrecognized format: {}".format(format))
    return mf, mat


def dump(filename, mf, mat, format):
    if format in format_registry and format_registry[format].exportable:
        io = format_registry[format]
        io.dump_manifest(filename+'.bedhm', mf)
        io.dump_matrix(filename, mf, mat)
    else:
        raise ValueError("Unrecognized format: {}".format(format))


def store_load(store, file_, key):
    if store in store_registry:
        with store_registry[store](file_) as io:
            mf = io.info(key)
            mat = io.get(key)
    else:
        raise ValueError("Storage container type not found: {}".format(store)) 
    return mf, mat


def store_save(store, file_, key, mf, mat):
    if store in store_registry:
        with store_registry[store](file_) as io:
            io.put(key, mf, mat)
    else:
        raise ValueError("Storage container type not found: {}".format(store))

