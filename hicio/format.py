from __future__ import division, print_function
from collections import defaultdict
import os
import glob
import errno
import gzip
import cPickle as pickle

import numpy as np
import scipy
import pandas
import h5py

from .manifest import Manifest
from .utils import strip_suffix


registry = {}


class FormatRegister(type):
    def __new__(mcl, name, bases, namespace):
        """ 
        name: name of class to construct (str)
        bases: base classes (tuple)
        namespace: dict

        """
        cls = type.__new__(mcl, name, bases, namespace)
        fmt_name = strip_suffix(name, 'Format').lower()
        if fmt_name:
            registry[fmt_name] = cls()
        return cls


class Format(object):
    __metaclass__ = FormatRegister    

    ext = ''
    mime = ''
    importable = True
    exportable = True

    def load_manifest(self, filename):
        return Manifest.from_bedfile(filename)

    def dump_manifest(self, filename, mf):
        mf.meta.update({'ftype': self.ext, 'mime': self.mime})
        mf.to_bedfile(filename)

    def load_matrix(self, filename):
        raise NotImplementedError

    def dump_matrix(self, filename, mf, mat):
        raise NotImplementedError


def from_extension(fp):
    import os
    base, ext = os.path.splitext(fp)
    if ext:
        for factory in registry.values():
            if factory.ext == ext[1:]:
                return factory
    raise ValueError("Could not determine storage format from file name.")


class TsvFormat(Format):
    ext = 'tsv'
    mime = 'text/plain'

    def load_matrix(self, filename, mf):
        return np.loadtxt(filename, dtype=mf.meta['dtype'])

    def dump_matrix(self, filename, mf, mat):
        fmt = None
        if 'f' in mf.meta['dtype']:
            fmt = '%.4f'
        elif 'i' in mf.meta['dtype']:
            fmt = '%d'
        np.savetxt(filename, mat, fmt=fmt)


class BedpeFormat(Format):
    ext = 'bedpe'
    mime = 'text/plain'

    columns = ['chrom1', 'start1', 'end1', 
               'chrom2', 'start2', 'end2', 
               'name', 'score']

    def load_matrix(self, filename, mf):
        nbins = len(mf.table)
        mat = np.zeros((nbins, nbins))
        bed = pandas.read_csv(filename, sep='\t')
        for k, row in bed.iterrows():
            i = mf.table.index[mf.table.start == row.start1]
            j = mf.table.index[mf.table.start == row.start2]
            mat[i,j] = mat[j,i] = row.score
        return mat

    def dump_matrix(self, filename, mf, mat):
        table = mf.table
        nbins = len(table)
        bin_size = table.end[0]-table.start[0]
        d = defaultdict(list)
        for i in xrange(nbins):
            for j in xrange(i, nbins):
                if mat[i,j] != 0:
                    d['chrom1'].append(table.chrom[i])
                    d['start1'].append(table.start[i])
                    d['end1'].append(table.end[i])
                    d['chrom2'].append(table.chrom[j])
                    d['start2'].append(table.start[j])
                    d['end2'].append(table.end[j])
                    d['name'].append('HiC')
                    d['score'].append(mat[i,j])
        bed = pandas.DataFrame(data=d, columns=self.columns)
        bed.to_csv(filename, sep='\t', index=None)


class NpyFormat(Format):
    ext = 'npy'
    mime = 'application/octet-stream'

    def load_matrix(self, filename, mf):
        return np.load(filename)

    def dump_matrix(self, filename, mf, mat):
        np.save(filename, mat)


