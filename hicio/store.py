from __future__ import division, print_function
from scipy.io import loadmat, savemat, whosmat
import numpy as np
import pandas
import h5py

from .manifest import Manifest
from .utils import strip_suffix


registry = {}


class StoreRegister(type):
    def __new__(mcl, name, bases, namespace):
        """ 
        name: name of class to construct (str)
        bases: base classes (tuple)
        namespace: dict

        """
        cls = type.__new__(mcl, name, bases, namespace)
        fmt_name = strip_suffix(name, 'Store').lower()
        if fmt_name:
            registry[fmt_name] = cls #factory
        return cls


class Store(object):
    __metaclass__ = StoreRegister
    ext = ''
    mime = ''

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.close()


def from_extension(fp):
    import os
    base, ext = os.path.splitext(fp)
    if ext:
        for factory in registry.values():
            if factory.ext == ext[1:]:
                return factory
    raise ValueError("Could not determine storage format from file name.")


class Hdf5Store(Store):
    ext = 'h5'
    mime = 'application/x-hdf'

    def __init__(self, name, *args, **kwargs):
        self._h5 = h5py.File(name, *args, **kwargs)
    
    def info(self, group_path):
        grp = self._h5[group_path]
        meta = dict(grp['info'].attrs.items())
        d = {}
        for name in grp['info'].iterkeys():
            d[name] = grp['info'][name].value
        mf = Manifest.from_dict(d, meta=meta)
        return mf       

    def get(self, group_path):
        grp = self._h5[group_path]
        return grp['data'].value

    def put(self, group_path, mf, mat):
        meta = mf.meta
        d = mf.table.to_dict(outtype='list')
        if group_path not in self._h5:
            self._h5.create_group(group_path)
            self._h5.create_group(group_path+'/info')
            self._h5.create_group(group_path+'/data')
        grp = self._h5[group_path]

        # put meta
        meta.update({
            'ftype': self.ext, 
            'mime': self.mime})
        for k, v in meta.items():
            grp['info'].attrs[k] = v
        
        # put frame
        for name in d:
            if name in grp['info']:
                del grp['info'][name]
            grp['info'][name] = d[name]

        # put matrix
        if 'data' in grp:
            del grp['data']
        grp['data'] = mat

    def iterkeys(self):
        for k, v in self._h5.iteritems():
            if 'data' in v and 'info' in v:
                yield k

    def iteritems(self):
        for k, v in self._h5.iteritems():
            if 'data' in v and 'info' in v:
                yield k, self.info(k), self.get(k)

    def close(self):
        self._h5.close()


class MatlabStore(Store):
    ext = 'mat'
    mime = 'application/octet-stream'

    def __init__(self, fp, *args, **kwargs):
        self._fp = fp

    def _manifest_key(self, key):
        return key + '_info'

    def _is_manifest_key(self, key):
        return key.endswith('_info')

    def _unbox(self, item):
        while True:
            if isinstance(item, basestring) or not hasattr(item, '__len__'):
                break
            try:
                item = item[0]
            except IndexError:
                item = ''
        return item

    def info(self, key):
        rec = loadmat(self._fp)[self._manifest_key(key)]
        
        rec_meta = rec['meta'][0][0]
        fields = rec_meta.dtype.names
        meta = {}
        for name in fields:
            meta[name] = self._unbox(rec_meta[name])

        rec_frame = rec['frame'][0][0]
        fields = rec_frame.dtype.names
        frame = {}
        for name in fields:
            frame[name] = [self._unbox(s) for s in rec_frame[name][0]]
        frame = pandas.DataFrame.from_dict(frame)

        mf = Manifest(meta)
        mf.table = frame
        return mf

    def get(self, key):
        return loadmat(self._fp)[key]

    def put(self, key, mf, mat):
        try:
            s = loadmat(self._fp)
        except (IOError, ValueError):
            s = {}

        mf.meta.update({
            'ftype': self.ext, 
            'mime': self.mime})

        # convert tabular data to recarrays --> matlab structs
        meta = np.array([tuple(mf.meta.values())], 
            dtype=[(k, 'O') for k in mf.meta.keys()])
        frame = mf.table.to_records(index=False)
        rec = np.array([(meta, frame)], 
            dtype=[('meta', 'O'), ('frame', 'O')])

        s[self._manifest_key(key)] = rec
        s[key] = mat

        return savemat(self._fp, s)

    def iterkeys(self):
        for name, shape, dtype in whosmat(self._fp):
            if not self._is_manifest_key(name):
                yield name

    def iteritems(self):
        s = loadmat(self._fp)
        for name, shape, dtype in whosmat(self._fp):
            if not self._is_manifest_key(name):
                yield name, self.info(name), s.get(name)

    def close(self):
        pass


class NpzStore(Store):
    ext = 'npz'
    mime = 'application/octet-stream'

    def __init__(self, fp, *args, **kwargs):
        from numpy.npyio import NpzFile
        self._npz = NpzFile(fp)

    def _manifest_key(self, key):
        return key + '_info'

    def _is_manifest_key(self, key):
        return key.endswith('_info')

    def info(self, key):
        return self._npz[self._manifest_key(key)]

    def get(self, key):
        return self._npz[key]

    def put(self, key, mf, mat):
        self._npz[self._manifest_key(key)] = mf
        self._npz[key] = mat

    def iterkeys(self):
        for k in self._npz.iterkeys():
            if not self._is_manifest_key(name):
                yield k

    def iteritems(self):
        for k, v in self._npz.iteritems():
            if not self._is_manifest_key(name):
                yield k, self.info(k), v

    def close(self):
        self._npz.close()
