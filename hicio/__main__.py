"""Hi-C I/O.

Usage:
    hicio convert <FILEPATH> --format <FORMAT> [--out <OUTPATH>]
    hicio list <STOREPATH>
    hicio dump <STOREPATH> --format <FORMAT> [--out <OUTPATH>]
    hicio dump <STOREPATH> <FIELD> --format <FORMAT> [--out <OUTPATH>]
    hicio add <FILEGLOB> <STOREPATH>
    hicio -h | --help
    hicio --version

Options:
    -h --help     Show this screen.
    --version     Show version.
    --format      File format for export.
    --out         Output base name (if different from input base name).
    --manifest <m>  Base name of the manifest file (if different from matrix file).

"""
from __future__ import division, print_function
import sys
import os
import glob

from . import __version__, load, dump
from . import format
from . import store
from docopt import docopt


def run(args):

    if args['convert']:
        in_filename = args['<FILEPATH>']
        base_name, in_ext = os.path.splitext(in_filename)
        out_ext = args.get('<FORMAT>')
        out_filename = '.'.join([base_name, out_ext])
        mf, mat = load(in_filename, format=in_ext[1:])
        dump(out_filename, mf, mat, format=out_ext)
        
    elif args['list']:
        fp = args['<STOREPATH>']
        factory = store.from_extension(fp)
        print("Contents of {}:".format(fp))
        with factory(fp, 'r') as io:
            for k, mf, mat in io.iteritems():
                print('\t{}\t{}'.format(k, mf.meta))

    elif args['dump']:
        fp = args['<STOREPATH>']
        factory = store.from_extension(fp)
        ext = args['<FORMAT>']
        with factory(fp, 'r') as io:
            if args['<FIELD>'] is None:
                for name in list(io.iterkeys()):
                    print("Unpacking {}...".format(name))
                    mf = io.info(name)
                    mat = io.get(name)
                    dump('.'.join([name,ext]), mf, mat, ext)
            else:
                name = args['<FIELD>']
                mf = io.info(name)
                mat = io.get(name)
                dump('.'.join([name,ext]), mf, mat, ext)

    elif args['add']:
        fp = args['<STOREPATH>']
        factory = store.from_extension(fp)
        files = glob.glob(args['<FILEGLOB>'])
        with factory(fp, mode='a') as io:
            for name in files:
                factory = format.from_extension(name)
                base, _ = os.path.splitext(name)
                mf, mat = load(name, format=factory.ext)
                print("Saving {}...".format(base))
                io.put(base, mf, mat)


def main():
    run(docopt(__doc__, version=__version__))


if __name__=='__main__':
    main()
