from __future__ import division, print_function
from functools import partial
import sys
import os

from nose.tools import assert_raises, with_setup
import numpy as np

from hicio.manifest import Manifest, symmetric_frame
from hicio import load, dump, store_load, store_save

path_to = partial(os.path.join, 
    os.path.join(
        os.path.dirname(
            os.path.abspath(__file__)), 'files')
)


def iter_examples():
    step = 1000
    n_bins = 10

    table = symmetric_frame(('chr22', 0, step*n_bins, step))
    matrix = np.zeros((n_bins,n_bins), dtype='i4')
    yield table, matrix, 'i4'

    table = symmetric_frame(('chr22', 0, step*n_bins, step))
    matrix = np.ones((n_bins,n_bins), dtype='i4')
    yield table, matrix, 'i4'

    # table = symmetric_frame(('chr22', 0, step*n_bins, step))
    # matrix = np.random.rand(n_bins,n_bins)
    # yield table, matrix, 'f8'
 

# def setup_func():
#     pass

# def teardown_func():
#     os.remove('temp.*')

def test_manifest():
    fp = path_to('example.bedhm')
    for table, matrix, dtype in iter_examples():
        manifest = Manifest.from_dataframe(table, {'dtype': dtype})
        manifest.to_bedfile(fp)
        manifest2 = Manifest.from_bedfile(fp)
        assert manifest.meta == manifest2.meta


def test_tsv():
    fp = path_to('example.tsv')
    for table, matrix, dtype in iter_examples():
        manifest = Manifest.from_dataframe(table, {'dtype': dtype})
        dump(fp, manifest, matrix, format='tsv')
        manifest2, matrix2 = load(fp, format='tsv')
        assert np.allclose(matrix, matrix2)


def test_bedpe():
    fp = path_to('example.bedpe')
    for table, matrix, dtype in iter_examples():
        manifest = Manifest.from_dataframe(table, {'dtype': dtype})
        dump(fp, manifest, matrix, format='bedpe')
        manifest2, matrix2 = load(fp, format='bedpe')
        assert np.allclose(matrix, matrix2)


def test_npy():
    fp = path_to('example.npy')
    for table, matrix, dtype in iter_examples():
        manifest = Manifest.from_dataframe(table, {'dtype': dtype})
        dump(fp, manifest, matrix, format='npy')
        manifest2, matrix2 = load(fp, format='npy')
        assert np.allclose(matrix, matrix2)


def test_hdf5():
    fp = path_to('example.h5')
    for i, (table, matrix, dtype) in enumerate(iter_examples()):
        manifest = Manifest.from_dataframe(table, {'dtype': dtype})
        store_save('hdf5', fp, 'example{}'.format(i), manifest, matrix)
        manifest2, matrix2 = store_load('hdf5', fp,'example{}'.format(i))
        assert np.allclose(matrix, matrix2)


def test_matlab():
    fp = path_to('example.mat')
    for i, (table, matrix, dtype) in enumerate(iter_examples()):
        manifest = Manifest.from_dataframe(table, {'dtype': dtype})
        store_save('matlab', fp, 'example{}'.format(i), manifest, matrix)
        manifest2, matrix2 = store_load('matlab', fp,'example{}'.format(i))
        assert np.allclose(matrix, matrix2)


